from experiments import run_experiment

experiments = [
    "constraints_monomapping_ones_intersection",
    "constraints_monomapping_ones_merge",
    "constraints_monomapping_ones_exclusive",
    "constraints_polimapping_ones",
    "random_polimapping",
    "random_monomapping",
]

NUM_EXEC = 10

for experiment_name in experiments:
    run_experiment("imdb", "mt", experiment_name, num_exec=NUM_EXEC)

for experiment_name in experiments:
    run_experiment("movielens", "th5", experiment_name, num_exec=NUM_EXEC)