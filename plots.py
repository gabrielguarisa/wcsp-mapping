import seaborn as sns
import pandas as pd

name = "cifar10_mt"
df = pd.read_csv("results/{}.csv".format(name))
fdf = df[
    df["Mapeamento"].isin(
        [
            "random_polimapping",
            "random_monomapping",
            "constraints_polimapping_lines",
            "constraints_monomapping_lines_merge",
            "constraints_monomapping_lines_intersection",
        ]
    )
]

pt_names = {
    "random_polimapping": "Aleatório independente",
    "random_monomapping": "Aleatório compartilhado",
    "constraints_polimapping_lines": "WCSP independente",
    "constraints_monomapping_lines_merge": "WCSP compartilhado (merge)",
    "constraints_monomapping_lines_intersection": "WCSP compartilhado (intersection)",
}

fdf.replace(pt_names, inplace=True)

ax = sns.barplot(x="n", y="Acurácia", hue="Mapeamento", data=fdf)
ax.figure.savefig("images/{}.png".format(name), figsize=(8, 6), dpi=300)
