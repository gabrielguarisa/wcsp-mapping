import wisardpkg as wp
import numpy as np
from datasets import get_dataset, get_shape
from datamanager import DataManager
from utils import get_random_score, get_mapping_score, valid_tuple_sizes
from wcsp import (
    get_restrictions_and_priorities,
    create_mapping_by_restrictions,
    get_merged_restrictions_and_priorities,
    get_intersection_restrictions_and_priorities,
    get_exlusive_restrictions_and_priorities,
    choice_priorities_to_priority_of_choice,
)
from finders import get_line_finder, get_diagonal_finder, OnesFinder
from abc import ABC, abstractmethod


class TemplateExperiment(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def run(self, tuple_size, ds):
        pass


class RandomPolimapping(TemplateExperiment):
    def __init__(self):
        super().__init__()

    def run(self, tuple_size, ds):
        return get_random_score(tuple_size, ds.train, ds.test, monomapping=False)


class RandomMonomapping(TemplateExperiment):
    def __init__(self):
        super().__init__()

    def run(self, tuple_size, ds):
        return get_random_score(tuple_size, ds.train, ds.test, monomapping=True)


class ConstraintsPolimapping(TemplateExperiment):
    def __init__(self, ds, finder):
        self.restrictions, self.priorities = get_restrictions_and_priorities(
            ds.train, finder, ds.get_name()
        )

        super().__init__()

    def run(self, tuple_size, ds):
        mapping = {}

        for label, choice_priorities in self.priorities.items():
            mapping[label] = create_mapping_by_restrictions(
                self.restrictions[label],
                choice_priorities_to_priority_of_choice(choice_priorities),
                tuple_size,
                ds.entry_size,
            )

        return get_mapping_score(mapping, ds.train, ds.test)


class ConstraintsMonomapping(TemplateExperiment):
    def __init__(self, ds, finder, operation_name):
        self.operation_func = None
        if operation_name.lower() == "merge":
            operation_func = get_merged_restrictions_and_priorities
        elif operation_name.lower() == "intersection":
            operation_func = get_intersection_restrictions_and_priorities
        elif operation_name.lower() == "exclusive":
            operation_func = get_exlusive_restrictions_and_priorities
        else:
            raise Exception("Invalid operation!")

        self.restrictions, self.choice_priorities = operation_func(
            ds.train, finder, ds.get_name()
        )

        super().__init__()

    def run(self, tuple_size, ds):
        mapping = create_mapping_by_restrictions(
            self.restrictions,
            choice_priorities_to_priority_of_choice(self.choice_priorities),
            tuple_size,
            ds.entry_size,
        )

        return get_mapping_score(mapping, ds.train, ds.test, monomapping=True)


def get_experiment(method_name, ds):
    method_info = method_name.split("_")

    experiment = None
    if method_info[0].lower() == "random":
        if method_info[1].lower() == "polimapping":
            experiment = RandomPolimapping()
        elif method_info[1].lower() == "monomapping":
            experiment = RandomMonomapping()
        else:
            raise Exception("Invalid experiment name!")
    elif method_info[0].lower() == "constraints":
        finder = None
        if method_info[2] == "lines":
            finder = get_line_finder(get_shape(ds.dataset_name))
        elif method_info[2] == "diagonal":
            finder = get_diagonal_finder(get_shape(ds.dataset_name))
        elif method_info[2] == "ones":
            finder = OnesFinder()
        else:
            raise Exception("Finder not found!")

        if method_info[1].lower() == "polimapping":
            experiment = ConstraintsPolimapping(ds, finder)
        elif method_info[1].lower() == "monomapping":
            experiment = ConstraintsMonomapping(ds, finder, method_info[3].lower())
        else:
            raise Exception("Invalid experiment name!")
    else:
        raise Exception("Invalid experiment name!")

    return experiment


def run_experiment(
    dataset_name, binarization_name, method_name, num_exec=30, folder="results",
):
    print("Method:", method_name)

    manager = DataManager(folder, dataset_name, binarization_name)
    ds = get_dataset(dataset_name, binarization_name)
    tuple_sizes = valid_tuple_sizes(ds.entry_size)
    new_tuple_sizes = []
    new_num_exec = []

    for tuple_size in tuple_sizes:
        temp_scores = manager.get_score(method_name, tuple_size)
        scores_size = len(temp_scores)
        if scores_size < num_exec:
            new_tuple_sizes.append(tuple_size)
            new_num_exec.append(num_exec - scores_size)

    if len(new_tuple_sizes) > 0:
        experiment = get_experiment(method_name, ds)
        for tuple_size in new_tuple_sizes:
            for _ in range(num_exec):
                score = experiment.run(tuple_size, ds)
                print("Tuple Size:", tuple_size, ":: Score:", score, end="\r")
                manager.append(method_name, tuple_size, score)

            manager.save()
