from experiments import run_experiment

datasets = ["mnist", "cifar10", "fashion"]
binarizations = ["mt"]
experiments = [
    "constraints_monomapping_lines_intersection",
    "constraints_monomapping_lines_merge",
    "constraints_monomapping_lines_exclusive",
    "constraints_polimapping_lines",
    "constraints_monomapping_diagonal_intersection",
    "constraints_monomapping_diagonal_merge",
    "constraints_monomapping_diagonal_exclusive",
    "constraints_polimapping_diagonal",
    "random_polimapping",
    "random_monomapping",
]

NUM_EXEC = 10

for binarization_name in binarizations:
    for dataset_name in datasets:
        print("Dataset: {}_{}".format(dataset_name, binarization_name))
        for experiment_name in experiments:
            run_experiment(
                dataset_name, binarization_name, experiment_name, num_exec=NUM_EXEC
            )
