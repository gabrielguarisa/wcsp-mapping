from utils import mkdir, file_exists
import pandas as pd


class DataManager:
    def __init__(self, folder, dataset_name, binarization_name):
        self.folder = mkdir(folder)
        self.dataset_name = dataset_name
        self.binarization_name = binarization_name

        self.load()

    def get_filename(self):
        return "{}/{}_{}.csv".format(
            self.folder, self.dataset_name, self.binarization_name
        )

    def get_data(self):
        return self._data

    def load(self):
        if file_exists(self.get_filename()):
            self._data = pd.read_csv(self.get_filename())
        else:
            self._data = pd.DataFrame(columns=["Mapeamento", "n", "Acurácia",])
            self._data = self._data.astype(
                {"Mapeamento": str, "n": int, "Acurácia": float,}
            )
        return self

    def save(self):
        self._data.to_csv(self.get_filename(), index=False)
        return self

    def append(self, method, tuple_size, accuracy):
        self._data = self._data.append(
            {"Mapeamento": method, "n": tuple_size, "Acurácia": accuracy,},
            ignore_index=True,
        )
        return self

    def get_filtered_data(
        self, method=None, tuple_size=None,
    ):
        fdf = self._data.copy()

        if method != None:
            fdf = fdf.loc[fdf["Mapeamento"] == method]

        if tuple_size != None:
            fdf = fdf.loc[fdf["n"] == tuple_size]

        return fdf

    def get_score(
        self, method=None, tuple_size=None,
    ):
        return self.get_filtered_data(method, tuple_size,)["Acurácia"].tolist()
