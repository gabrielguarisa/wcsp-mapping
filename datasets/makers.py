import numpy as np
from .dataset import Dataset
from .preprocessing import grayscale, LabelBinarizer
from cifar10_web import cifar10
from mnist import get_mnist, get_fashion_mnist
import wisardpkg as wp
import pandas as pd
from .remakeckp import fromPickle
from vega_datasets import data as iris_data
from sklearn.model_selection import train_test_split


def make_iris(binarization_name, folder):
    iris = iris_data.iris()

    X = iris.drop(columns=["species"]).values.tolist()
    y = iris["species"]
    
    train_images, test_images, train_labels, test_labels = train_test_split(
        X, y, test_size=0.33
    )

    return Dataset.binarize(
        np.copy(train_images),
        np.copy(train_labels),
        np.copy(test_images),
        np.copy(test_labels),
        "iris",
        binarization_name,
        num_classes=3,
        entry_size=128,
    ).save(folder)


def make_raw_dataset(binarization_name, folder, raw_folder, dataset_name):
    train = fromPickle("{}{}_train.pkl".format(raw_folder, dataset_name))
    test = fromPickle("{}{}_test.pkl".format(raw_folder, dataset_name))

    num_classes = len(np.unique(train["labels"]))
    entry_size = len(train["images"][0].ravel())

    return Dataset.binarize(
        train["images"],
        train["labels"],
        test["images"],
        test["labels"],
        dataset_name,
        binarization_name,
        num_classes=num_classes,
        entry_size=entry_size,
    ).save(folder)


def make_cifar10(binarization_name, folder):
    train_images, train_labels, test_images, test_labels = cifar10()

    train_images = train_images.reshape([-1, 3, 32, 32])
    test_images = test_images.reshape([-1, 3, 32, 32])

    # move the channel dimension to the last
    train_images = np.rollaxis(train_images, 1, 4)
    test_images = np.rollaxis(test_images, 1, 4)

    train_images_gray = np.array(grayscale(train_images) * 255, dtype="int")
    test_images_gray = np.array(grayscale(test_images) * 255, dtype="int")

    lb = LabelBinarizer()
    lb.fit(list(range(10)))
    new_train_labels = lb.inverse_transform(train_labels)
    new_test_labels = lb.inverse_transform(test_labels)

    return Dataset.binarize(
        train_images_gray,
        new_train_labels,
        test_images_gray,
        new_test_labels,
        "cifar10",
        binarization_name,
        num_classes=10,
    ).save(folder)


def make_mnist(binarization_name, folder):
    train_images, train_labels, test_images, test_labels = get_mnist()

    return Dataset.binarize(
        np.copy(train_images),
        np.copy(train_labels),
        np.copy(test_images),
        np.copy(test_labels),
        "mnist",
        binarization_name,
        num_classes=10,
        entry_size=784,
    ).save(folder)


def make_fashion(binarization_name, folder):
    train_images, train_labels, test_images, test_labels = get_fashion_mnist()

    return Dataset.binarize(
        np.copy(train_images),
        np.copy(train_labels),
        np.copy(test_images),
        np.copy(test_labels),
        "fashion",
        binarization_name,
        num_classes=10,
        entry_size=784,
    ).save(folder)
