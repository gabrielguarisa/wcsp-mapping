import os
from .dataset import Dataset
from .makers import make_cifar10, make_mnist, make_fashion, make_raw_dataset, make_iris
import wisardpkg as wp


def get_dataset(dataset_name, binarization_name, folder="datasets/data/"):
    if folder[-1] != "/":
        folder = "{}/".format(folder)

    filename = "{}{}/{}_info.ds".format(folder, dataset_name, binarization_name)
    if os.path.isfile(filename):
        print("Loading:", filename)
        return Dataset.load(dataset_name, binarization_name, folder)
    elif dataset_name == "cifar10":
        return make_cifar10(binarization_name, folder)
    elif dataset_name == "mnist":
        return make_mnist(binarization_name, folder)
    elif dataset_name == "fashion":
        return make_fashion(binarization_name, folder)
    elif dataset_name == "ckp":
        return make_raw_dataset(
            binarization_name, folder, "{}raw/".format(folder), "ckp"
        )
    elif dataset_name == "iris":
        return make_iris(binarization_name, folder)
    elif dataset_name == "imdb":
        return Dataset(
            wp.DataSet("{}imdb/imdb_train_mt.wpkds".format(folder)),
            wp.DataSet("{}imdb/imdb_test_mt.wpkds".format(folder)),
            "imdb",
            "mt",
        )
    elif dataset_name == "movielens":
        return Dataset(
            wp.DataSet("{}movielens/movielens_train.wpkds".format(folder)),
            wp.DataSet("{}movielens/movielens_test.wpkds".format(folder)),
            "movielens",
            "th5",
        )
    else:
        raise Exception("Dataset not found!")


def get_shape(dataset_name):
    shapes = {
        "mnist": (28, 28),
        "fashion": (28, 28),
        "cifar10": (32, 32),
        "ckp": (100, 100),
    }

    return shapes[dataset_name]
