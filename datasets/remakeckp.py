import glob
import os
import pickle
import numpy as np

from PIL import Image
import wisardpkg as wp

from skimage.filters import threshold_sauvola
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt


def loadLandmarks(filename):
    landmarks = []
    with open(filename) as f:
        for line in f.readlines():
            x = int(float(line.split()[0]))
            y = int(float(line.split()[1]))
            landmarks.append((x, y))
        f.close()
    return landmarks


def getLandmarks(filelandmarks):
    landmarks = loadLandmarks(filelandmarks)
    rect = [-1, -1, -1, -1]
    for _, (x, y) in enumerate(landmarks):
        if x < rect[0] or rect[0] == -1:
            rect[0] = x
        if x > rect[2] or rect[2] == -1:
            rect[2] = x
        if y < rect[1] or rect[1] == -1:
            rect[1] = y
        if y > rect[3] or rect[3] == -1:
            rect[3] = y

    return rect


def toPickle(data, filename):
    with open(filename, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def fromPickle(filename):
    with open(filename, 'rb') as handle:
        return pickle.load(handle)


def ckpData():
    imgs_dir = 'data/cohn-kanade-images/'
    landmarks_dir = 'data/Landmarks/'
    emotion_dir = 'data/Emotion/'

    labelsc = {}
    i = 0

    images = []
    labels = []
    for emotion_filename in glob.glob("{}**/**/*.txt".format(emotion_dir)):
        f = open(emotion_filename, "r")
        label = str(int(float(f.read().strip())))
        if label not in labelsc:
            labelsc[label] = 0

        labelsc[label] += 1

        img_filename = imgs_dir + \
            emotion_filename[len(emotion_dir):-len("_emotion.txt")] + ".png"

        img = Image.open(img_filename)
        if len(np.shape(img)) == 3:
            img = Image.open(img_filename).convert('L')

        rect = getLandmarks(
            landmarks_dir + img_filename[len(imgs_dir):-len(".png")] + '_landmarks.txt')
        img = img.crop(rect)
        img = np.array(img.resize((100, 100), Image.ANTIALIAS))

        images.append(img)
        labels.append(label)

        print(i, end="\r")
        i += 1

    return images, labels


def ckpToPickle():
    images, labels = ckpData()

    images_train, images_test, labels_train, labels_test = train_test_split(
        images, labels, test_size=0.2, random_state=42)

    train = {"images": images_train, "labels": labels_train}
    test = {"images": images_test, "labels": labels_test}

    print("TRAIN SIZE: {}".format(len(train["images"])))
    print(np.array(np.unique(train["labels"], return_counts=True)).T)
    print("TEST SIZE: {}".format(len(test["images"])))
    print(np.array(np.unique(test["labels"], return_counts=True)).T)

    toPickle(train, "ckp_train.pkl")
    toPickle(test, "ckp_test.pkl")


if __name__ == "__main__":
    ckpToPickle()
